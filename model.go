package chopper

import (
	"encoding/json"
	"log"

	"github.com/fatih/structs"
	"github.com/sirupsen/logrus"
)

type Chopper struct {
	// data logging general
	SessionId         string `json:"session_id"`
	CompanyId         int    `json:"company_id"`
	Severity          string `json:"severity"`
	ReqBodyPostToCore string `json:"req_body_post_to_core"`
	ReqEndpointToCore string `json:"req_endpoint_to_core"`
	RespBodyToClient  string `json:"resp_body_to_client"`
	// data logging from client
	ClientRequestId string `json:"client_request_id"`
	ClientVersion   string `json:"client_version"`
	ClientReqBody   string `json:"client_req_body"`
	// data logging from odoo
	CoreMessage  string `json:"core_message"`
	CoreRespBody string `json:"core_resp_body"`
}

func (c *Chopper) WriteLog() {
	m := structs.Map(c)
	logrus.WithFields(m)

}

func (c *Chopper) SetSessionId(sessionId string) {
	c.SessionId = sessionId
}
func (c *Chopper) SetCompanyId(companyId int) {
	c.CompanyId = companyId
}
func (c *Chopper) SetSeverity(level int) {
	severity := GetSeverity(level)

	c.Severity = severity
}
func GetSeverity(level int) (severity string) {
	switch level {
	case 0:
		severity = "DEFAULT"
	case 100:
		severity = "DEBUG"
	case 200:
		severity = "INFO"
	case 300:
		severity = "NOTICE"
	case 400:
		severity = "WARNING"
	case 500:
		severity = "ERROR"
	case 600:
		severity = "CRITICAL"
	case 700:
		severity = "ALERT"
	case 800:
		severity = "EMERGENCY"
	}

	return
}
func (c *Chopper) SetReqBodyPostToCore(body string) {
	c.ReqBodyPostToCore = body
}
func (c *Chopper) SetReqEndpointPostToCore(endpoint string) {
	c.ReqBodyPostToCore = endpoint
}
func (c *Chopper) SetRespBodyToClient(body interface{}) {
	data, err := json.Marshal(body)
	if err != nil {
		log.Println(err)
	}
	c.RespBodyToClient = string(data)
}
func (c *Chopper) SetClientRequestId(requestId string) {
	c.ClientRequestId = requestId
}
func (c *Chopper) SetClientVersion(clientVersion string) {
	c.ClientVersion = clientVersion
}
func (c *Chopper) SetClientReqBody(body string) {
	c.ClientReqBody = body
}
func (c *Chopper) SetCoreMessage(message string) {
	c.CoreMessage = message
}
func (c *Chopper) SetCoreRespBody(body string) {
	c.CoreRespBody = body
}

func (c *Chopper) GetResponse(code int, message string, data interface{}) (resp Response) {
	resp.Code = code
	resp.Id = c.ClientRequestId
	resp.Message = message
	resp.Result = data
	c.SetRespBodyToClient(resp)
	return
}
