package chopper

import (
	"encoding/json"
	"fmt"
	"strings"
)

type ErrMessage struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type ErrorCustom struct {
	Jsonrpc string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Result  struct {
		Error struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		} `json:"error"`
	} `json:"result"`
}

type ErrorBehaviour struct {
	Jsonrpc string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Result  struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		Data    struct {
			Name          string   `json:"name"`
			Debug         string   `json:"debug"`
			Message       string   `json:"message"`
			Arguments     []string `json:"arguments"`
			ExceptionType string   `json:"exception_type"`
			Context       struct {
			} `json:"context"`
		} `json:"data"`
	} `json:"result"`
}

func ParseErr(dataIn interface{}) (data ErrMessage, err error) {
	var errCustom ErrorCustom
	byteData, _ := json.Marshal(dataIn)
	err = json.Unmarshal(byteData, &errCustom)
	if err != nil {
		return
	}
	if errCustom.Result.Error.Code > 0 {
		data.Code = errCustom.Result.Error.Code
		data.Message = errCustom.Result.Error.Message
	} else {
		var errBehaviour ErrorBehaviour
		err = json.Unmarshal(byteData, &errBehaviour)
		if err != nil {
			return
		}
		if errBehaviour.Result.Code > 0 {
			data.Code = errBehaviour.Result.Code
			message := fmt.Sprintf("%v, field [%v]", errBehaviour.Result.Message, errBehaviour.Result.Data.Message)
			data.Message = message
		}
	}
	return
}

func CheckingError(data []byte) (check bool) {
	index := strings.Index(string(data), "error")
	if index > 0 {
		check = true
	}
	return
}
